package mygame;

import java.util.List;

/**
 *
 * @author Daniel Strong
 */
public class MiniMaxAi implements Ai {

        public Board getNextMoveForPlayer(Board board, int player) {
                if (player == 1) {
                        return getMaximizedMove(board);
                } else if (player == 2) {
                        return getMinimizedMove(board);
                }
                throw new IllegalArgumentException("invalid player: " + player);
        }

        private Board getMaximizedMove(Board board) {
                Board bestMove = null;
                Integer bestScore = null;
                List<Board> availableMoves = board.getAvailableMoves(1);
                for (Board move : availableMoves) {
                        int winner = move.getWinner();
                        int score;
                        if (winner > 0) {
                                score = move.getWinnerAsScore();
                        } else {
                                Board minned = getMinimizedMove(move);
                                score = minned.getWinnerAsScore();
                        }

                        if (bestScore == null || score > bestScore) {
                                bestMove = move;
                                bestScore = score;
                        }
                }
                return bestMove;
        }

        private Board getMinimizedMove(Board board) {
                Board bestMove = null;
                Integer bestScore = null;
                List<Board> availableMoves = board.getAvailableMoves(2);
                for (Board move : availableMoves) {
                        int winner = move.getWinner();
                        int score;
                        if (winner > 0) {
                                score = move.getWinnerAsScore();
                        } else {
                                Board maxxed = getMaximizedMove(move);
                                score = maxxed.getWinnerAsScore();
                        }

                        if (bestScore == null || score < bestScore) {
                                bestMove = move;
                                bestScore = score;

                        }
                }

                return bestMove;

        }

}
