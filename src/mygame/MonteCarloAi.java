package mygame;

import com.jme3.math.FastMath;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Daniel Strong
 */
public class MonteCarloAi implements Ai {

        private int playouts;

        public MonteCarloAi() {
                playouts = 1000;
        }

        public MonteCarloAi(int playouts) {
                this.playouts = playouts;
        }

        public Board getNextMoveForPlayer(Board board, int player) {
                List<Board> availableMoves = board.getAvailableMoves(player);
                float bestValue = Float.MIN_VALUE;
                Board bestMove = null;

                for (Board move : availableMoves) {
                        float value = evaluateBoard(move, player);
                        if (value > bestValue || bestMove == null) {
                                bestValue = value;
                                bestMove = move;
                        }
                }

                return bestMove;
        }

        private float evaluateBoard(Board board, int player) {
                if (board.getWinner() == player) {
                        return 10;
                } else if (board.getWinner() != 3 && board.getWinner() != 0) {
                        return -10;
                } else if (board.getWinner() == 3) {
                        return 0;
                }
                int value = 0;
                for (int ply = 0; ply < playouts; ply++) {
                        int otherPlayer = (player == 1 ? 2 : 1);
                        Board expand = expand(board, otherPlayer);
                        if (expand.getWinner() == player) {
                                value += 1;
                        } else if (expand.getWinner() != 3 && expand.getWinner() != 0) {
                                value -= 1;
                        } else if (expand.getWinner() == 3) {
                                value += 0;
                        }
                }
                System.out.println((float) value / (float) playouts);
                return (float) value / (float) playouts;
        }

        private Board expand(Board board, int player) {
                List<Board> availableMoves = board.getAvailableMoves(player);
                Collections.shuffle(availableMoves);
                Board get = availableMoves.get(0);
                if (get.getWinner() > 0) {
                        return get;
                } else {
                        return expand(get, (player == 1 ? 2 : 1));
                }
        }

}
