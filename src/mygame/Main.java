package mygame;

/**
 * Tic tac toe with minimax ai
 *
 * @author Daniel Strong
 */
public class Main {

        public static void main(String[] args) {
                Main main = new Main();
        }

        public Main() {
                Ai player1Ai = new MonteCarloAi(10);
                Ai player2Ai = new MonteCarloAi(1000);
                int currentPlayer = 1;
                Board board = new Board();
                //board = board.placeTile(2, 0, 1); // do X's move for him
                //currentPlayer = 2;
                board.printBoard();

                while (true) {
                        System.out.println("it is player " + currentPlayer + "'s turn.\n");
                        if (currentPlayer == 1) {
                                board = player1Ai.getNextMoveForPlayer(board, currentPlayer);
                        } else if (currentPlayer == 2) {
                                board = player2Ai.getNextMoveForPlayer(board, currentPlayer);
                        }
                        board.printBoard();

                        int winner = board.getWinner();
                        if (board.getWinner() > 0) {
                                if (winner < 3) {
                                        System.out.println("Player " + winner + " wins.");
                                } else {
                                        System.out.println("Tie");
                                }

                                return;
                        }

                        currentPlayer = (currentPlayer == 1 ? 2 : 1);
                }
        }

}
