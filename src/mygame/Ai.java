package mygame;

/**
 *
 * @author Daniel Strong
 */
public interface Ai {

        public Board getNextMoveForPlayer(Board board, int player);

}
