package mygame;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Daniel Strong
 */
public class Board implements Cloneable {

        private int[][] tiles; // 0 = empty space, 1 = X, 2 = O

        public Board() {
                tiles = new int[3][3];
        }

        public Board(Board copy) {
                tiles = new int[3][3];

                for (int y = 0; y < 3; y++) {
                        for (int x = 0; x < 3; x++) {
                                tiles[x][y] = copy.tiles[x][y];
                        }
                }
        }

        public Board placeTile(int x, int y, int tile) {
                Board clone = clone();
                clone.tiles[x][y] = tile;
                return clone;
        }

        public int getWinnerAsScore() {
                int winner = getWinner();
                if (winner == 1) {
                        return 10;
                } else if (winner == 2) {
                        return -10;
                } else {
                        return 0;
                }
        }

        public int getWinner() {
                // 0 no winner, 1 x wins, 2 o wins, 3 tie
                for (int y = 0; y < 3; y++) {
                        if (tiles[0][y] == tiles[1][y] && tiles[1][y] == tiles[2][y]) {
                                return tiles[0][y]; // row
                        }
                }
                for (int x = 0; x < 3; x++) {
                        if (tiles[x][0] == tiles[x][1] && tiles[x][1] == tiles[x][2]) {
                                return tiles[x][0]; // col
                        }
                }
                if (tiles[0][2] == tiles[1][1] && tiles[1][1] == tiles[2][0]) {
                        return tiles[0][2]; //NW to SE
                }
                if (tiles[2][2] == tiles[1][1] && tiles[1][1] == tiles[0][0]) {
                        return tiles[2][2]; // NE to SW
                }

                for (int x = 0; x < 3; x++) {
                        for (int y = 0; y < 3; y++) {
                                if (tiles[x][y] == 0) {
                                        return 0;
                                }
                        }
                }

                return 3;
        }

        public List<Board> getAvailableMoves(int player) {
                List<Board> moves = new LinkedList<Board>();

                for (int x = 0; x < 3; x++) {
                        for (int y = 0; y < 3; y++) {
                                if (tiles[x][y] == 0) {
                                        // clones board with added move, add to the list
                                        moves.add(placeTile(x, y, player));
                                }
                        }
                }

                return moves;
        }

        private String getTileAsString(int x, int y) {
                int tile = tiles[x][y];
                if (tile == 0) {
                        return " ";
                } else if (tile == 1) {
                        return "X";
                } else if (tile == 2) {
                        return "O";
                } else {
                        throw new AssertionError(tile);
                }
        }

        public void printBoard() {
                for (int y = 2; y >= 0; y--) {
                        System.out.println(String.format("\t %s | %s | %s", getTileAsString(0, y), getTileAsString(1, y), getTileAsString(2, y)));
                        if (y > 0) {
                                System.out.println("\t-----------");
                        }
                }
        }

        @Override
        public Board clone() {
                return new Board(this);
        }

}
